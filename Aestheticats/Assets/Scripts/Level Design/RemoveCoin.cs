﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveCoin : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Bank")
        {
            
            Destroy(gameObject);
        }
    }

}
