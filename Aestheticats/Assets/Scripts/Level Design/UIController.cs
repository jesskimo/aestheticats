﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour {

    public GameObject m_blackoutSquare;
    public bool b_isDead = false;

    void Update()
    {
        if (b_isDead == true)
        {
            StartCoroutine(FadeBlackoutSquare());
        }
        else
        {
            return;
        }
    }

    public IEnumerator FadeBlackoutSquare(bool b_fadeToBlack = true, int m_fadeSpeed = 2)
    {
        Color m_objectColor = m_blackoutSquare.GetComponent<SpriteRenderer>().color;
        float m_fadeAmount;

        if (b_fadeToBlack)
        {
            while (m_objectColor.a < 1)
            {
                m_fadeAmount = m_objectColor.a + (m_fadeSpeed * Time.deltaTime);
                m_objectColor = new Color(m_objectColor.r, m_objectColor.g, m_objectColor.b, m_fadeAmount);
                m_blackoutSquare.GetComponent<SpriteRenderer>().color = m_objectColor;
                yield return null;
            }
        }
    }
}
