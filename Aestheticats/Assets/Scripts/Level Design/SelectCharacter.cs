﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SelectCharacter : MonoBehaviour
{
    public GameObject playerSpriteRenderer;
    public Sprite selectedSkin;
    public RuntimeAnimatorController skinAnimator;

    public static SelectCharacter characterSelection; // Allows variables to be used in other scripts

    public void SelectSkin()
    {
        playerSpriteRenderer.GetComponent<SpriteRenderer>().sprite = selectedSkin;
        playerSpriteRenderer.GetComponent<Animator>().runtimeAnimatorController = skinAnimator;
        SceneManager.LoadScene(1); // Loads World 1
    }
}
