﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Currency : MonoBehaviour {

    public int currency;
    
    public Text text;
    public AudioSource coinPickup;

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Currency")
        {
            coinPickup.Play();
            currency += 1;
        }
    }

    public void Update()
    {
        text.text = currency.ToString();
    }
}
