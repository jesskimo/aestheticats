﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public GameObject pauseMenu;
    Scene currentScene;

	void Awake ()
    {
        pauseMenu.SetActive(false);
        currentScene = SceneManager.GetActiveScene(); // The current level
    }
	
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            // Shows the pause menu and freezes the game
            if(pauseMenu.activeInHierarchy == false)
            {
                pauseMenu.SetActive(true);
                Time.timeScale = 0f;
            }
            // Hides the pause menu if it's already active
            else
            {
                Resume();
            }
        }
	}

    public void Resume()
    {
        pauseMenu.SetActive(false);
        Time.timeScale = 1f; // Unfreezes the game
        Debug.Log("Resume");
    }

    public void Restart()
    {
        SceneManager.LoadScene(currentScene.buildIndex); // Retrieves the build index of the scene and loads it
        Time.timeScale = 1f;
        Debug.Log("Restart");
    }

    public void Quit()
    {
        Application.Quit();
        Debug.Log("Quit");
    }
}
