﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMaster : MonoBehaviour
{
    public GameObject player;

    void Start()
    {
        player.GetComponent<SpriteRenderer>().sprite = SelectCharacter.characterSelection.selectedSkin; // Selects the skin the player has picked
        player.GetComponent<Animator>().runtimeAnimatorController = SelectCharacter.characterSelection.skinAnimator; // Sets animator controller according to skin
    }
}
