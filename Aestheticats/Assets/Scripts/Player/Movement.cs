﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{

    public float speed;
    public float jumpHeight;
    public AudioSource jump;

    public float fallMultiplier;

    [SerializeField]
    private float m_slopeCheckDistance;
    [SerializeField]
    private Transform m_groundCheck;
    [SerializeField]
    private LayerMask m_whatIsGround;
    [SerializeField]
    private float m_groundCheckRadius;
    [SerializeField]
    private PhysicsMaterial2D m_noFriction;
    [SerializeField]
    private PhysicsMaterial2D m_fullFriction;
    
    



    [SerializeField]
    private bool m_isGrounded;
    private bool m_isOnSlope;
    private bool m_isJumping;
    private bool m_canJump;

    private float m_slopeDownAngle;
    private float m_slopeDownAngleOld;
    private float m_slopeSideAngle;

    

    Rigidbody2D rb;
    private CapsuleCollider2D cc;

    public Animator animator;
    public Transform sprite;
   

    private Vector2 m_colliderSize;
    private Vector2 m_slopeNormalPerp;
    private Vector2 m_newVelocity;
    private Vector2 m_newForce;

    Vector3 move;
    float horizontalMove;
    
    public bool m_isDead = false;
    
    public bool facingRight;
    
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        cc = GetComponent<CapsuleCollider2D>();

        m_colliderSize = cc.size;

        facingRight = true;
    }

    void Update()
    {
        
        horizontalMove = Input.GetAxisRaw("Horizontal");
        
        
        animator.SetFloat("Speed", Mathf.Abs (horizontalMove));
        OnLanding();
        Jump();
        
    }

   

    void FixedUpdate()
    {
        Flip(horizontalMove);
        SlopeCheck();
        ApplyMovement();
        CheckGround();
        
    }

    private void SlopeCheck()
    {
        Vector2 m_checkPos = transform.position - new Vector3(0.0f, m_colliderSize.y / 2);

        SlopeCheckVertical(m_checkPos);
        SlopeCheckHorizontal(m_checkPos);
    }

    private void SlopeCheckHorizontal(Vector2 m_checkPos)
    {
        RaycastHit2D slopeHitFront = Physics2D.Raycast(m_checkPos, transform.right, m_slopeCheckDistance, m_whatIsGround);
        RaycastHit2D slopeHitBack = Physics2D.Raycast(m_checkPos, -transform.right, m_slopeCheckDistance, m_whatIsGround);

        if (slopeHitFront)
        {
            m_isOnSlope = true;
            m_slopeSideAngle = Vector2.Angle(slopeHitFront.normal, Vector2.up);
        }
        else if (slopeHitBack)
        {
            m_isOnSlope = true;
            m_slopeSideAngle = Vector2.Angle(slopeHitBack.normal, Vector2.up);
        }
        else
        {
            m_slopeSideAngle = 0.0f;
            m_isOnSlope = false;
        }
    }

    private void SlopeCheckVertical(Vector2 m_checkPos)
    {
        RaycastHit2D hit = Physics2D.Raycast(m_checkPos, Vector2.down, m_slopeCheckDistance, m_whatIsGround);

        if (hit)
        {
            m_slopeNormalPerp = Vector2.Perpendicular(hit.normal).normalized;
            m_slopeDownAngle = Vector2.Angle(hit.normal, Vector2.up);
            if (m_slopeDownAngle != m_slopeDownAngleOld)
            {
                m_isOnSlope = true;
            }
            m_slopeDownAngleOld = m_slopeDownAngle;
            Debug.DrawRay(hit.point, m_slopeNormalPerp, Color.red);
            Debug.DrawRay(hit.point, hit.normal, Color.green);
        }

        if (m_isOnSlope && horizontalMove == 0.0f)
        {
            rb.sharedMaterial = m_fullFriction;
        }
        else
        {
            rb.sharedMaterial = m_noFriction;
        }
    }

    private void ApplyMovement()
    {
        if (m_isGrounded && !m_isOnSlope && !m_isJumping)
        {
            m_newVelocity.Set(speed * horizontalMove, 0.0f);
            rb.velocity = m_newVelocity;
        }
        else if(m_isGrounded && m_isOnSlope && !m_isJumping)
        {
            m_newVelocity.Set(speed * m_slopeNormalPerp.x * -horizontalMove, speed * m_slopeNormalPerp.y * -horizontalMove);
            rb.velocity = m_newVelocity;
        }
        else if (!m_isGrounded)
        {
            m_newVelocity.Set(speed * horizontalMove /2.8f , rb.velocity.y);
            rb.velocity = m_newVelocity;
        }
    }

    

    private void Jump()
    {
        

        if (Input.GetKey(KeyCode.W) && m_isGrounded)
        {
            if (m_canJump)
            {
                
                m_canJump = false;
                m_isJumping = true;
                m_newVelocity.Set(0.0f, 0.0f);
                rb.velocity = m_newVelocity;
                m_newForce.Set(0.0f, jumpHeight);
                rb.AddForce(m_newForce, ForceMode2D.Impulse);
                jump.Play();
                animator.SetBool("isJumping", true);
                animator.SetTrigger("Jump");
            }
        }

        if (rb.velocity.y < 0)
        {
            //rb.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
        }
    }

    void CheckGround()
    {
        m_isGrounded = Physics2D.OverlapCircle(m_groundCheck.position, m_groundCheckRadius, m_whatIsGround);

        if(rb.velocity.y <= 0.0f)
        {
            m_isJumping = false;
        }

        if (m_isGrounded && !m_isJumping)
        {
            m_canJump = true;
        }
    }

    

    void OnLanding()
    {
        if (m_isGrounded)
        {
            animator.SetBool("isJumping", false);

        }
    }

    private void Flip(float horizontalMove)
    {
        if (horizontalMove > 0 && !facingRight || horizontalMove <0 && facingRight)
        {
            facingRight = !facingRight;
            Vector3 theScale = sprite.transform.localScale;
            theScale.x *= -1;
            sprite.transform.localScale = theScale;
            
            
        }
    }
}

