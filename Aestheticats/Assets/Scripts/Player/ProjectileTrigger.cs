﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileTrigger : MonoBehaviour {

    //attach this script to any object that is capable of triggering projectiles

    public GameObject m_projectile; //this is the reference to your projectile prefab
    public Transform m_spawnTransform; //this is the reference to the transfrom where the prefab will spawn

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Instantiate(m_projectile, m_spawnTransform.position, m_spawnTransform.rotation);
        }
    }
}
