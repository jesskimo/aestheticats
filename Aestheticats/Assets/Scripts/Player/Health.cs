﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour {

    
    [Range(0, 100)] public int health = 100;

    public Image stage1, stage2, stage3, stage4;
    

    public AudioSource backgroundMusic;
    public AudioSource defeatMusic;

    public void Start()
    {
        stage1.enabled = true;
    }

    public void Update()
    {
        if (gameObject != null)
        {
            if (health <= 0)
            {
                Debug.Log("Dead");
            }
        }

        if (health >33 && health <= 66)
        {
            stage1.enabled = false;
            stage2.enabled = true;
        }
        if (health > 0 && health <= 32)
        {
            stage2.enabled = false;
            stage3.enabled = true;
        }
        if (health <= 0)
        {
            stage3.enabled = false;
            stage4.enabled = true;
        }

        FallOnDeath();
    }

    void FallOnDeath()
    {
        if (health <= 1)
        {
            GetComponent<BoxCollider2D>().enabled = false;
            GetComponent<Movement>().enabled = false;
            backgroundMusic.Stop();
            defeatMusic.Play();
        }
    }

}
