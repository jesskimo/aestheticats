﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    //attach this script to the projectile prefab

    GameObject m_playerObject; //reference to player's movement script

    public float m_speed = 10f; //this is the projectile's speed
    public float m_lifespan = 3f; //this is the projectile's lifespan (in seconds)
    public int m_damage = 34; //this is the projectile's damage
    public AudioSource m_sfx; //this is the sound clip that plays when the projectile has spawned

    private Rigidbody2D m_rb;

    //called when the script instance is being loaded
    void Awake()
    {
        m_rb = GetComponent<Rigidbody2D>();
        m_playerObject = GameObject.FindGameObjectWithTag("Player");
    }

    // Use this for initialization
    void Start ()
    {
        if (m_playerObject.GetComponent<Movement>().facingRight == true) //checking if the player is facing right or left to determine the projectile direction 
        {
            m_rb.velocity = transform.right * m_speed;
            Vector3 m_theScale = transform.localScale;
            m_theScale.x *= 1;
            transform.localScale = m_theScale;
        }
        else
        {
            m_rb.velocity = transform.right * m_speed * -1;
            Vector3 m_theScale = transform.localScale;
            m_theScale.x *= -1;
            transform.localScale = m_theScale;
        }
        Destroy(gameObject, m_lifespan);
        if (gameObject != null)
        {
            m_sfx.Play();
        }
	}

    void OnTriggerEnter2D(Collider2D hitInfo)
    {
        E_Health c_enemy = hitInfo.GetComponent<E_Health>();
        if (c_enemy != null)
        {
            c_enemy.TakeDamage(m_damage);
        }
        if (hitInfo.tag != "Currency" && hitInfo.tag != "Decoration")
        {
            Destroy(gameObject);
        }
    }


   
}
