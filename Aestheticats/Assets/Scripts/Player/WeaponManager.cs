﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponManager : MonoBehaviour
{
    public GameObject currentWeapon;
    public GameObject[] inventory; // Creates an array to store weapons

	void Start ()
    {
        inventory[0] = currentWeapon; // Adds the first weapon to the inventory
        Debug.Log("Added first weapon");
	}
	
    // Adds a weapon to the inventory when the player touches it
	void OnCollisionEnter2D(Collision collision)
    {
		if(collision.collider.tag == "Weapon")
        {
            inventory[inventory.Length + 1] = collision.collider.gameObject;
            collision.collider.gameObject.SetActive(false); // Hides the weapon
            Debug.Log("Added to inventory");
        }
	}

    void Update()
    {
        // Switches to the next weapon
        if (Input.GetKeyDown(KeyCode.P))
        {
            int nextWeapon = System.Array.IndexOf(inventory, currentWeapon) + 1; // Looks for the current weapon's position in the array
            currentWeapon = inventory[nextWeapon];
            Debug.Log("Switched weapon");

            // Cycles back to the first weapon if there are no more weapons in the inventory
            if(nextWeapon > inventory.Length)
            {
                currentWeapon = inventory[0];
                Debug.Log("Switched to first weapon");
            }
        }
    }
}
