﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class P_Health : MonoBehaviour {

    public int m_health = 100; //sets player's total health 
    public Image m_imageA, m_imageB, m_imageC, m_imageD; //images that will be cycled through depending on player's current health
    public AudioSource m_backgroundAudio, m_defeatAudio; //sounds that are involved in player's death
    public Animator m_animator;
    public GameObject m_mainCamera;
    public Renderer playerSprite;


    public float m_flashTime = 0.5f;
    public GameObject m_enemySprite;
    SpriteRenderer m_enemySpriteRender;
    Color m_resetColor;

    public void Start()
    {
        m_enemySpriteRender = m_enemySprite.GetComponent<SpriteRenderer>();
        m_resetColor = m_enemySpriteRender.color;
    }


    // Update is called once per frame
    void Update () {
		if (gameObject != null)
        {
            HealthStatus();
        }
        if (m_health <= 0)
        {
            GetComponent<BoxCollider2D>().enabled = false;
            GetComponent<CircleCollider2D>().enabled = false;
            GetComponent<CapsuleCollider2D>().enabled = false;
            GetComponent<Movement>().enabled = false;
            m_animator.SetBool("isDead", true);
            m_backgroundAudio.Stop();
            playerSprite.sortingOrder = 9;
            m_mainCamera.GetComponent<UIController>().b_isDead = true;
        }
	}

    public void TakeDamage (int damage)
    {
        m_health -= damage;
        m_enemySpriteRender.color = Color.red;
        Invoke("ResetColor", m_flashTime);
        if (m_health <= 0)
        {
            m_defeatAudio.Play();

        }
    }

    void ResetColor()
    {
        m_enemySpriteRender.color = m_resetColor;
    }

    private void HealthStatus()
    {
        if (m_health >= 67 && m_health <= 100)
        {
            return;
        }
        else if (m_health >33 && m_health <= 66)
        {
            m_imageA.enabled = false;
            m_imageB.enabled = true;
        }
        else if (m_health > 0 && m_health <= 32)
        {
            m_imageA.enabled = false;

            m_imageB.enabled = false;
            m_imageC.enabled = true;
        }
        else
        {
            m_imageA.enabled = false;
            m_imageC.enabled = false;
            m_imageD.enabled = true;
        }
    }
}
