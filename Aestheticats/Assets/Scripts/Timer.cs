﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour {

    public Text text;
    float timer;

    public void Update()
    {
        timer += Mathf.Abs(Time.deltaTime);
        text.text = timer.ToString();
    }
}
