﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitHealthInfo : MonoBehaviour
{
    public int maxHP;
    public int currentHP;

    public void TakeDamage()
    {
        currentHP -= Random.Range(10, 21); // The unit will lose between 10 and 20 HP

        if (currentHP < 0)
            currentHP = 0; // Ensures that the unit's health isn't less than 0

        if (currentHP == 0)
            Destroy(gameObject);
        else
            return;
    }

    void Start()
    {
        currentHP = maxHP;
    }
}
