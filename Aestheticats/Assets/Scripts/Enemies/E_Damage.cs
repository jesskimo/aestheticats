﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class E_Damage : MonoBehaviour {

    //attach onto enemies that will deal damage to the player upon collision with player

    public int m_damage = 15; //enemies damage to deal to player

    void OnTriggerEnter2D(Collider2D m_hitInfo)
    {
        P_Health c_player = m_hitInfo.GetComponent<P_Health>();
        if (c_player != null)
        {
            c_player.TakeDamage(m_damage);
        }
    }
}
