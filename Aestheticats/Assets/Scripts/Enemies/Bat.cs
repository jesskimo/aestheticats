﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bat : MonoBehaviour {


    [Range(1, 15)]
    public float velocity;

    public Transform sightStart;
    public Transform sightEnd;
    public Rigidbody2D rb;

    public LayerMask detectWhat;

    public bool colliding;

    public void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    public void Update()
    {
        rb.velocity = new Vector2(velocity * -1, rb.velocity.y);

        colliding = Physics2D.Linecast(sightStart.position, sightEnd.position, detectWhat);

        if (colliding)
        {
            transform.localScale = new Vector2(transform.localScale.x * -1, transform.localScale.y);
            velocity *= -1; 
        }

        
    }
    

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(sightStart.position, sightEnd.position);
    }

    
}
