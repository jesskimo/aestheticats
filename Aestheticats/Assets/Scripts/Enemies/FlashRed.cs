﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlashRed : MonoBehaviour {

    public float m_flashTime = 0.5f;
    public GameObject m_enemySprite;
    SpriteRenderer m_enemySpriteRender;
    Color m_resetColor;

	// Use this for initialization
	void Start ()
    {
        m_enemySpriteRender = m_enemySprite.GetComponent<SpriteRenderer>();
        m_resetColor = m_enemySpriteRender.color;
	}
	
	void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Projectile" || collision.tag =="Enemy")
        {
            m_enemySpriteRender.color = Color.red;
            Invoke("ResetColor", m_flashTime);
        }
    }

    void ResetColor()
    {
        m_enemySpriteRender.color = m_resetColor;
    }
}
