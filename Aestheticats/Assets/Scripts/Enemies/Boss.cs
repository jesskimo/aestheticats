﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour {

    public Transform player;
   

    public bool isFlipped = false;

    public Transform tp1;
    public Transform tp2;
    public Transform sightStart, sightEnd;
    bool isRight;
    Rigidbody2D rb;
    public float velocity;
    public LayerMask detectWhat;
    public bool colliding;

    public HealthBar healthBar;

    public void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        healthBar.SetMaxHealth(GetComponent<E_Health>().m_health);
    }

    public void Update()
    {
        healthBar.SetHealth(GetComponent<E_Health>().m_health);
    }

    public void LookAtPlayer()
    {
        Vector3 flipped = transform.localScale;
        flipped.z *= -1f;

        if (transform.position.x > player.position.x && isFlipped)
        {
            transform.localScale = flipped;
            transform.Rotate(0f, 180f, 0f);
            isFlipped = false;
        }
        else if (transform.position.x < player.position.x && !isFlipped)
        {
            transform.localScale = flipped;
            transform.Rotate(0f, 180f, 0f);
            isFlipped = true;
        }
    }
    public void Teleport()
    {
        Vector2 left = new Vector2(tp2.position.x, transform.position.y);
        Vector2 right = new Vector2(tp1.position.x, transform.position.y);
        isRight = !isRight;

        if (isRight)
        {
            transform.position = left;
        }
        else
        {
            transform.position = right;

        }
    }

    public void RunPhase()
    {
        
        if (isFlipped)
        {
            rb.velocity = new Vector2(velocity * 1, rb.velocity.y);
        }
        else
        {
            rb.velocity = new Vector2(velocity * -1, rb.velocity.y);
        }
        colliding = Physics2D.Linecast(sightStart.position, sightEnd.position, detectWhat);

        if (colliding)
        {
            transform.localScale = new Vector2(transform.localScale.x * -1, transform.localScale.y);
            velocity *= -1;
        }
    }

    public void setFinalPhase()
    {
        rb.velocity = new Vector2(0f, 0f);
        
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(sightStart.position, sightEnd.position);
    }

}
