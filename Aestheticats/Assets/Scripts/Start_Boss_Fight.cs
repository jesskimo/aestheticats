﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Start_Boss_Fight : MonoBehaviour {

    public GameObject boss, healthBar;
    public AudioSource speedUpMusic;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        boss.SetActive(true);
        healthBar.SetActive(true);
        speedUpMusic.pitch = 1.25f;
    }
}
