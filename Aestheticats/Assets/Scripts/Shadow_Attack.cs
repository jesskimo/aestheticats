﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shadow_Attack : MonoBehaviour {

    public int damage;
    GameObject playerGO;
    Transform player;
    public bool isFlipped = false;


  

    public void Start()
    {
        playerGO = GameObject.FindGameObjectWithTag("Player");
        player = GameObject.FindGameObjectWithTag("Player").transform;
      

    }

    public void Attack()
    {
        playerGO.GetComponent<P_Health>().TakeDamage(damage);
    }

    

   

    public void LookAtPlayer()
    {
        Vector3 flipped = transform.localScale;
        flipped.z *= -1f;

        if (transform.position.x > player.position.x && isFlipped)
        {
            transform.localScale = flipped;
            transform.Rotate(0f, 180f, 0f);
            isFlipped = false;
        }
        else if (transform.position.x < player.position.x && !isFlipped)
        {
            transform.localScale = flipped;
            transform.Rotate(0f, 180f, 0f);
            isFlipped = true;
        }
    }
}
