﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shadow_Monster : StateMachineBehaviour {

    E_Health health;
    Transform player;
    Rigidbody2D rb;
    Shadow_Attack enemy;
    public float speed = 2.5f;
    public float attackRange = 3f, walkRange = 10f;

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        health = animator.GetComponent<E_Health>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
        rb = animator.GetComponent<Rigidbody2D>();
        enemy = animator.GetComponent<Shadow_Attack>();
	}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        enemy.LookAtPlayer();



        Vector2 target = new Vector2(player.position.x, rb.position.y);
        Vector2 newPos = Vector2.MoveTowards(rb.position, target, speed * Time.fixedDeltaTime);

        
        
        if (Vector2.Distance(player.position, rb.position) <= walkRange)
        {
            enemy.LookAtPlayer();
            rb.MovePosition(newPos);
           
        }
        else
        {
            animator.SetTrigger("Idle");
        }


       
        
        if (Vector2.Distance(player.position, rb.position) <= attackRange)
        {
            animator.SetTrigger("Attack");
        }


        if (health.m_health <= 0)

        {
            animator.SetTrigger("Death");
            animator.GetComponent<Rigidbody2D>().velocity = new Vector2(0f, 0f);
        }

	}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.ResetTrigger("Attack");
	}

	
}
