﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossWeapon : MonoBehaviour {

    
    public GameObject boss;
    
    public float speed = 10f; //this is the projectile's speed
    public float lifespan = 3f; //this is the projectile's lifespan (in seconds)
    public int damage = 34; //this is the projectile's damage
    public AudioSource sfx; //this is the sound clip that plays when the projectile has spawned

    Rigidbody2D rb;

    public void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    public void Start()
    {
        if (boss.GetComponent<Boss>().isFlipped == true)
        {
            rb.velocity = transform.right * speed;
            
        }
        else
        {
            rb.velocity = transform.right * speed * -1;
            
        }
        Destroy(gameObject, lifespan);
        if (gameObject != null)
        {
            sfx.Play();
        }
    }

    public void OnTriggerEnter2D(Collider2D hitInfo)
    {
        P_Health player = hitInfo.GetComponent<P_Health>();
        if (player != null)
        {
            player.TakeDamage(damage);
        }
        if (hitInfo.tag != "Decoration" && hitInfo.tag != "Bank")
        {
            Destroy(gameObject);
        }
    }
}
