﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Final_Phase : StateMachineBehaviour {

    Transform player;
    Rigidbody2D rb;
    Boss boss;
    public float timeBtwTp = 4f;
    public float maxTimeBtwTp = 4f;
    public float timeBtwRun = 4f;
    public float maxTimeBtwRun = 4f;
    public bool running;
    
        

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        rb = animator.GetComponent<Rigidbody2D>();
        boss = animator.GetComponent<Boss>();
        boss.setFinalPhase();
    }

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        
        boss.LookAtPlayer();
        //timeBtwTp -= Time.fixedDeltaTime;
        //timeBtwRun -= Time.fixedDeltaTime;
        //if (timeBtwTp <= 0)
        //{
            //animator.SetTrigger("Disappear");
            
            //timeBtwTp = maxTimeBtwTp;
       // }

        //if (timeBtwRun <= 0)
        //{
            //animator.SetTrigger("Run");
            //timeBtwRun = maxTimeBtwRun;
        //}
       
        
    }

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
	
	}

}
