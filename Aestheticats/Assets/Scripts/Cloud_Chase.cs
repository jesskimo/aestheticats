﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cloud_Chase : MonoBehaviour {

    Rigidbody2D rb;
    
    
    // Use this for initialization
	void Start ()
    {
        rb = GetComponent<Rigidbody2D>();	
	}
	
	// Update is called once per frame
	void Update ()
    {
        rb.velocity = new Vector2(4f, rb.velocity.y);
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.tag == "Player")
        {
            collision.GetComponent<P_Health>().TakeDamage(100);
            Debug.Log(collision.name);
        }
    }
}
