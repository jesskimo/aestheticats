﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss_Throw : StateMachineBehaviour {

    Transform player;
    Rigidbody2D rb;
    Boss boss;
    public float timeBtwTp = 4f;
    public float maxTimeBtwTp = 4f;

    public int phase2health;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        rb = animator.GetComponent<Rigidbody2D>();
        boss = animator.GetComponent<Boss>();
    }

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        boss.LookAtPlayer();
        timeBtwTp -= Time.fixedDeltaTime;
        if (timeBtwTp <= 0)
        {
            animator.SetTrigger("Disappear");
            //boss.Teleport();
            timeBtwTp = maxTimeBtwTp;
        }

        if (animator.GetComponent<E_Health>().m_health <= phase2health)
        {
            //animator.GetComponent<E_Health>().m_health = phase2health;
            animator.SetTrigger("Run");
        }

    }

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
	
	}
}
