﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAttack : MonoBehaviour {

    public Transform player;
    public GameObject projectile;
    public Transform spawnTransform;

    public void Attack()
    {
        //StartCoroutine("ThrowAttack");
    }

    IEnumerator ThrowAttack()
    {
        Instantiate(projectile, spawnTransform.position, spawnTransform.rotation);
        yield return new WaitForSeconds(5);
    }
}
