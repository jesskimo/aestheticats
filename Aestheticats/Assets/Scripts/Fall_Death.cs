﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fall_Death : MonoBehaviour {

    public GameObject player;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            player.GetComponent<P_Health>().TakeDamage(100);

        }
       
    }
}
