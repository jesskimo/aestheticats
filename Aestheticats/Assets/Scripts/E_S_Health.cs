﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class E_S_Health : MonoBehaviour {

    //attach this script to enemies

    public int m_health = 100; //enemy's total health points
    public Animator animator;

    public void TakeDamage(int damage)
    {
        m_health -= damage;
        if (m_health <= 0)
        {
            animator.SetBool("isDead", true);
            //Destroy(gameObject);
        }
    }

}
